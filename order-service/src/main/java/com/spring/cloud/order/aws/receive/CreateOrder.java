package com.spring.cloud.order.aws.receive;

import com.spring.cloud.order.aws.sns.Sns;
import com.spring.cloud.order.domain.Order;
import com.spring.cloud.order.domain.Order.SubOrder;
import java.util.Collections;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.Acknowledgment;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Component
public class CreateOrder {

  @Autowired
  Sns sns;

  @SqsListener(value = "create-order", deletionPolicy = SqsMessageDeletionPolicy.NEVER)
  public void receiveMessage(Order order, Acknowledgment acknowledgment) {

    System.out.println("Received the command create-order \n"
        + "----------------------------- \n"
        + "Queue: create-order \n"
        + "Message: " + order.toString() + "\n");

    final var subOrder = new SubOrder();
    subOrder.setUuid(UUID.randomUUID());
    subOrder.setDescription("SubOrder Description");

    order.setSubOrders(Collections.singletonList(subOrder));

    sns.send("order-created", order, "created-order");
    acknowledgment.acknowledge();
  }
}
