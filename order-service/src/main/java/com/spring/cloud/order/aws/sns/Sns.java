package com.spring.cloud.order.aws.sns;

import com.spring.cloud.order.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

@Component
public class Sns {

  @Autowired
  NotificationMessagingTemplate messagingTemplate;

  public void send(String topic, Order message, String subject) {
    try {
      messagingTemplate
          .sendNotification(topic, message, subject);
    }
    catch (MessagingException messagingException) {
      throw new MessagingException("Message failed");
    }
  }
}
