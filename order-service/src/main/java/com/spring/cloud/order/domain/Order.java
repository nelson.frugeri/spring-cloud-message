package com.spring.cloud.order.domain;

import java.util.List;
import java.util.UUID;

public class Order {

  private Integer id;

  private String name;

  private List<SubOrder> subOrders;

  public Order() {}

  public Order(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  static public class SubOrder {

    private UUID uuid;

    private String description;

    public SubOrder() {}

    public SubOrder(UUID uuid, String description) {
      this.uuid = uuid;
      this.description = description;
    }

    public UUID getUuid() {
      return uuid;
    }

    public String getDescription() {
      return description;
    }

    public void setUuid(UUID uuid) {
      this.uuid = uuid;
    }

    public void setDescription(String description) {
      this.description = description;
    }
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<SubOrder> getSubOrders() {
    return subOrders;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSubOrders(List<SubOrder> subOrders) {
    this.subOrders = subOrders;
  }

  @Override
  public String toString() {
    return "Order{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
