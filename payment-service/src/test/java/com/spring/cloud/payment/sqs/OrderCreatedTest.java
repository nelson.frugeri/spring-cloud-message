package com.spring.cloud.payment.sqs;

import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.SQS;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.spring.cloud.payment.aws.receiver.OrderCreated;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
@SpringBootTest
@TestPropertySource(properties = {"spring.config.location=classpath:application-test.yml"})
public class OrderCreatedTest {

  @Autowired
  private QueueMessagingTemplate queueMessagingTemplate;

  @Autowired
  private OrderCreated orderCreated;

  private static final String QUEUE_NAME = "test-queue";

  @Container
  public static LocalStackContainer localStack =
      new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.13.0"))
          .withServices(SQS);

  @TestConfiguration
  static class AwsTestConfig {

    @Bean
    public AmazonSQSAsync amazonSQS() {
      return AmazonSQSAsyncClientBuilder.standard()
          .withCredentials(localStack.getDefaultCredentialsProvider())
          .withEndpointConfiguration(localStack.getEndpointConfiguration(SQS))
          .build();
    }
  }

  @SneakyThrows
  @BeforeAll
  static void beforeAll() {
    localStack.execInContainer("awslocal", "sqs", "create-queue",
        "--queue-name", QUEUE_NAME);
  }

  @SneakyThrows
  @AfterAll
  static void afterAll() {
    localStack.execInContainer("awslocal", "sqs", "delete-queue",
        "--queue-name", QUEUE_NAME);
  }

  @Test
  void whenReceiveMessageIsFive() {
    queueMessagingTemplate.send(QUEUE_NAME, new GenericMessage<>("test"));
    await().untilAsserted(() -> orderCreated.receiveMessage(any(), any()));
  }
}
