package com.spring.cloud.payment.aws.receiver;

import org.springframework.cloud.aws.messaging.config.annotation.NotificationMessage;
import org.springframework.cloud.aws.messaging.listener.Acknowledgment;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Component
public class OrderCreated {

  @SqsListener(value = "payment-order-created", deletionPolicy = SqsMessageDeletionPolicy.NEVER)
  public void receiveMessage(@NotificationMessage String message, Acknowledgment acknowledgment) {

    System.out.println("Receive the event order-created \n"
        + "----------------------------- \n"
        + "Queue: payment-order-created \n"
        + "Message: " + message + "\n");

    acknowledgment.acknowledge();
  }
}
