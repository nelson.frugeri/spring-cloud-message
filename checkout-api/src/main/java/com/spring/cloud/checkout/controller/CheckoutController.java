package com.spring.cloud.checkout.controller;

import com.spring.cloud.checkout.adapter.aws.sqs.Sqs;
import com.spring.cloud.checkout.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/checkout")
public class CheckoutController {

  @Autowired
  private Sqs sqs;

  @ResponseBody
  @PostMapping
  public ResponseEntity<Order> create(@RequestBody Order order) {
    sqs.send("create-order", order);

    return ResponseEntity.status(HttpStatus.CREATED).body(order);
  }

  @ResponseBody
  @PostMapping("/batch")
  public ResponseEntity<String> createBatch() {

    for (int i = 1; i <= 50; i++) {
      sqs.send("create-order",
          new Order(i, "This is the order: " + i));
    }

    return ResponseEntity.status(HttpStatus.CREATED).body("Successfully batch orders created!");
  }
}
