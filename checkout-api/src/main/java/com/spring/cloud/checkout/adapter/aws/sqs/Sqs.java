package com.spring.cloud.checkout.adapter.aws.sqs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class Sqs {

  @Autowired
  QueueMessagingTemplate queueMessagingTemplate;

  public void send(String queueName, Object message) {
    queueMessagingTemplate.convertAndSend(queueName, message);
  }
}
