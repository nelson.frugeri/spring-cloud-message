package com.spring.cloud.checkout.domain;

import java.util.List;

public class Order {

  private Integer id;

  private String name;

  private List<SubOrder> subOrders;

  public Order(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  static public class SubOrder {

    private Integer id;

    private String description;

    public SubOrder(Integer id, String description) {
      this.id = id;
      this.description = description;
    }

    public Integer getId() {
      return id;
    }

    public String getDescription() {
      return description;
    }

    public void setId(Integer id) {
      this.id = id;
    }

    public void setDescription(String description) {
      this.description = description;
    }
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<SubOrder> getSubOrders() {
    return subOrders;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSubOrders(List<SubOrder> subOrders) {
    this.subOrders = subOrders;
  }

  @Override
  public String toString() {
    return "Order{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
